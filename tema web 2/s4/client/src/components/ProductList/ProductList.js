import React from 'react';

export class ProductList extends React.Component {
    
    transformItems = () => {
                return this.props.source.map((element, index) => {
                    return <div key={index}>{element.productName} with the price of {element.price}</div>
                })
    }
    delete(){
        console.log("test");
    }
    
    render(){
        let items = this.transformItems();
        
        return (
            <div>
                {items}
            </div>
            );
    }    
}